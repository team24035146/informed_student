package ru.team2.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.team2.api.dto.subject.SubjectCreateDto;
import ru.team2.api.mapper.SubjectMapper;
import ru.team2.domain.entities.Subject;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class SubjectMapperTests {
    @Autowired
    private SubjectMapper subjectMapper;

    @Test
    public void subjectToSubjectResponseDto() {
        // given
        var subject = new Subject().setId(1).setName("SubjectName");

        // when
        var subjectResponseDto = subjectMapper.subjectToSubjectResponseDto(subject);

        // then
        assertEquals(1, subjectResponseDto.getId());
        assertEquals("SubjectName", subjectResponseDto.getName());
    }

    @Test
    public void subjectsListToSubjectsResponseDtoList() {
        // given
        var subject1 = new Subject().setId(1).setName("SubjectName1");
        var subject2 = new Subject().setId(2).setName("SubjectName2");
        var subjects = List.of(subject1, subject2);

        // when
        var subjectsResponseDtoList = subjectMapper.subjectsListToSubjectsResponseDtoList(subjects);

        // then
        for (int i = 0; i < subjects.size(); i++) {
            assertEquals(subjects.get(i).getId(), subjectsResponseDtoList.get(i).getId());
            assertEquals(subjects.get(i).getName(), subjectsResponseDtoList.get(i).getName());
        }
    }

    @Test
    public void subjectCreateDtoToSubject() {
        // given
        var subjectCreateDto = new SubjectCreateDto().setCourseId(1).setName("SubjectName");

        // when
        var subject = subjectMapper.subjectCreateDtoToSubject(subjectCreateDto);

        // then
        assertEquals(1, subject.getCourse().getId());
        assertEquals("SubjectName", subject.getName());
    }
}

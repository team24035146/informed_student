package ru.team2.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.team2.api.mapper.CourseMapper;
import ru.team2.domain.entities.Course;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CourseMapperTests {
    @Autowired
    private CourseMapper courseMapper;

    @Test
    public void courseToCourseResponseDto() {
        // given
        var course = new Course().setId(1).setName("CourseName");

        // when
        var courseResponseDto = courseMapper.courseToCourseResponseDto(course);

        // then
        assertEquals(1, courseResponseDto.getId());
        assertEquals("CourseName", courseResponseDto.getName());
    }

    @Test
    public void coursesListToCoursesResponseDtoList() {
        // given
        var course1 = new Course().setId(1).setName("CourseName1");
        var course2 = new Course().setId(2).setName("CourseName1");
        var courses = List.of(course1, course2);

        // when
        var coursesResponseDtoList = courseMapper.coursesListToCoursesResponseDtoList(courses);

        // then
        for (int i = 0; i < courses.size(); i++) {
            assertEquals(courses.get(i).getId(), coursesResponseDtoList.get(i).getId());
            assertEquals(courses.get(i).getName(), coursesResponseDtoList.get(i).getName());
        }
    }
}

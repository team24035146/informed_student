package ru.team2.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.team2.api.dto.task.TaskCreateDto;
import ru.team2.api.mapper.TaskMapper;
import ru.team2.domain.entities.Task;
import ru.team2.domain.entities.enums.CategoryType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TaskMapperTests {
    @Autowired
    private TaskMapper taskMapper;

    @Test
    public void taskToTaskResponseDto() {
        // given
        var task = new Task().setId(1).setCategoryType(CategoryType.TEST);

        // when
        var taskResponseDto = taskMapper.taskToTaskResponseDto(task);

        // then
        assertEquals(1, taskResponseDto.getId());
        assertEquals("TEST", taskResponseDto.getCategoryType());
    }

    @Test
    public void tasksListToTasksResponseDtoList() {
        // given
        var task1 = new Task().setId(1).setCategoryType(CategoryType.CONSPECT);
        var task2 = new Task().setId(2).setCategoryType(CategoryType.LECTURE);
        var tasks = List.of(task1, task2);

        // when
        var taskResponseDtoList = taskMapper.tasksListToTasksResponseDtoList(tasks);

        // then
        for (int i = 0; i < tasks.size(); i++) {
            assertEquals(tasks.get(i).getId(), taskResponseDtoList.get(i).getId());
            assertEquals(tasks.get(i).getCategoryType().toString(), taskResponseDtoList.get(i).getCategoryType());
        }
    }

    @Test
    public void taskCreateDtoToTask() {
        // given
        var taskCreateDto = new TaskCreateDto().setSubjectId(1).setCategoryType("TEST");

        // when
        var task = taskMapper.taskCreateDtoToTask(taskCreateDto);

        // then
        assertEquals(1, task.getSubject().getId());
        assertEquals(CategoryType.TEST, task.getCategoryType());
    }
}

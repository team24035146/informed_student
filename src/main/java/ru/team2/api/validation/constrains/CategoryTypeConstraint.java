package ru.team2.api.validation.constrains;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import ru.team2.api.validation.validators.CategoryTypeValidator;

import java.lang.annotation.*;


@Documented
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CategoryTypeValidator.class)
public @interface CategoryTypeConstraint {
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
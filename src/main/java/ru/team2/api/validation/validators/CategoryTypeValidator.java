package ru.team2.api.validation.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import ru.team2.api.validation.constrains.CategoryTypeConstraint;
import ru.team2.domain.entities.enums.CategoryType;

public class CategoryTypeValidator implements ConstraintValidator<CategoryTypeConstraint, String> {
    @Override
    public boolean isValid(String model, ConstraintValidatorContext constraintValidatorContext) {
        try {
            CategoryType.valueOf(model);
        }
        catch (IllegalArgumentException illegalArgumentException) {
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate("Указанного типа таски не существует")
                    .addConstraintViolation();
            return false;
        }

        return true;
    }
}
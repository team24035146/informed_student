package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.subject.SubjectCreateDto;
import ru.team2.api.dto.subject.SubjectResponseDto;
import ru.team2.api.dto.task.TaskResponseDto;
import ru.team2.api.mapper.SubjectMapper;
import ru.team2.api.mapper.TaskMapper;
import ru.team2.domain.services.SubjectService;
import ru.team2.domain.services.TaskService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "subjects", description = "Контроллер для работы с предметами курса")
@RequestMapping("/subjects")
public class SubjectController {
    private final SubjectService subjectService;
    private final TaskService taskService;
    private final SubjectMapper subjectMapper;
    private final TaskMapper taskMapper;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Добавление предмета")
    public SubjectResponseDto addSubject(@RequestBody @Valid SubjectCreateDto subjectCreateDto) {
        var subject = subjectMapper.subjectCreateDtoToSubject(subjectCreateDto);

        return subjectMapper.subjectToSubjectResponseDto(subjectService.saveSubject(subject));
    }

    @GetMapping("/{subjectId}/tasks")
    @Operation(description = "Получение всех тасков по ID предмета")
    public List<TaskResponseDto> getAllTasksBySubjectId(@PathVariable @Schema(example = "1") Integer subjectId) {
        var tasks = taskService.getTasksBySubjectId(subjectId);

        return taskMapper.tasksListToTasksResponseDtoList(tasks);
    }

    @DeleteMapping("/{id}")
    @Operation(description = "Удаление предмета по ID")
    public void deleteSubject(@PathVariable @Schema(example = "1") Integer id) {
        subjectService.deleteSubject(id);
    }
}

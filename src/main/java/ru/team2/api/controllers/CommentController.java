package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.expression.AccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.team2.api.dto.comment.CommentCreateDto;
import ru.team2.api.dto.comment.CommentResponseDto;
import ru.team2.api.dto.comment.CommentUpdateDto;
import ru.team2.api.dto.comment.ThreadCommentCreateDto;
import ru.team2.api.dto.page.PageResponseDto;
import ru.team2.api.mapper.CommentMapper;
import ru.team2.api.mapper.PageMapper;
import ru.team2.domain.entities.User;
import ru.team2.domain.services.CommentService;
import ru.team2.domain.services.JwtService;
import ru.team2.domain.services.UserService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "comments", description = "Контроллер для работы с комментариями")
@RequestMapping("/comments")
public class CommentController {
    private final UserService userService;
    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final JwtService jwtService;
    private final PageMapper pageMapper;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Добавление комментария")
    public CommentResponseDto addComment(@RequestBody @Valid CommentCreateDto commentCreateDto,
                                         @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var authorEmail = jwtService.getEmailFromToken(jwtToken);
        var comment = commentMapper.commentCreateDtoToComment(commentCreateDto);

        comment.setUser(new User());
        comment.getUser().setEmail(authorEmail);

        return commentMapper.commentToCommentResponseDto(commentService.saveComment(comment));
    }

    @DeleteMapping("/{id}")
    @Operation(description = "Удаление комментария по ID")
    public void deleteComment(@PathVariable @Schema(example = "1") Integer id) {
        commentService.deleteComment(id);
    }

    @PutMapping()
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Обновление комментария")
    public CommentResponseDto updateComment(@RequestBody @Valid CommentUpdateDto commentUpdateDto,
                                            @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var authorEmail = jwtService.getEmailFromToken(jwtToken);
        var user = userService.getUserByEmail(authorEmail);
        var comment = commentService.getComment(commentUpdateDto.getId());
        if (!comment.getUser().getEmail().equals(authorEmail)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not the owner of the comment");
        }
        comment = commentMapper.commentUpdateDtoToComment(commentUpdateDto);
        comment.setUser(user);

        return commentMapper.commentToCommentResponseDto(commentService.updateComment(comment));
    }

    @GetMapping("/{id}/revisions")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Просмотр изменений комментария")
    public List<CommentResponseDto> getRevisions(@PathVariable @Schema(example = "1") Integer id) {
        return commentMapper.commentsListToCommentsResponseDtoList(commentService.getRevisions(id));
    }

    @GetMapping("/{commentId}/thread")
    @Operation(description = "Получение треда по ID комментария")
    public PageResponseDto<CommentResponseDto> getAllCommentsByPostId(@PathVariable @Schema(example = "1") Integer commentId,
                                                                      @RequestParam @Min(0) Integer page,
                                                                      @RequestParam @Min(1) @Max(50) Integer limit) {
        var comments = commentService.getThreadByCommentId(commentId, page, limit);
        PageResponseDto<CommentResponseDto> responseDto = pageMapper.toPageResponseDto(comments);
        responseDto.setContent(comments.getContent().stream().map(commentMapper::commentToCommentResponseDto).toList());

        return responseDto;
    }

    @PostMapping("/thread")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Добавление коммента в тред")
    public CommentResponseDto addThread(@RequestBody @Valid ThreadCommentCreateDto threadCommentCreateDto,
                                        @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) throws AccessException {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var authorEmail = jwtService.getEmailFromToken(jwtToken);
        var threadComment = commentMapper.threadCommentCreateDtoToComment(threadCommentCreateDto);

        threadComment.setUser(new User());
        threadComment.getUser().setEmail(authorEmail);

        return commentMapper.commentToCommentResponseDto(commentService.saveThreadComment(threadComment));
    }
}

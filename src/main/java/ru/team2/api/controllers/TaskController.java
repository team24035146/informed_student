package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.page.PageResponseDto;
import ru.team2.api.dto.post.PostResponseDto;
import ru.team2.api.dto.task.TaskCreateDto;
import ru.team2.api.dto.task.TaskResponseDto;
import ru.team2.api.mapper.PageMapper;
import ru.team2.api.mapper.PostMapper;
import ru.team2.api.mapper.TaskMapper;
import ru.team2.domain.services.PostService;
import ru.team2.domain.services.TaskService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "tasks", description = "Контроллер для работы с тасками")
@RequestMapping("/tasks")
public class TaskController {
    private final TaskService taskService;
    private final PostService postService;
    private final TaskMapper taskMapper;
    private final PostMapper postMapper;
    private final PageMapper pageMapper;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Добавление таска")
    public TaskResponseDto addTask(@RequestBody @Valid TaskCreateDto taskCreateDto) {
        var task = taskMapper.taskCreateDtoToTask(taskCreateDto);

        return taskMapper.taskToTaskResponseDto(taskService.saveTask(task));
    }

    @GetMapping("/category-types")
    @Operation(description = "Получение всех типов тасков")
    public List<String> getAllCategoryTypes() {
        return taskService.getAllCategoryTypes();
    }

    @GetMapping("/{taskId}/posts")
    @Operation(description = "Получение всех постов по ID таска")
    public PageResponseDto<PostResponseDto> getAllPostsByTaskId(@PathVariable @Schema(example = "1") Integer taskId,
                                                                @RequestParam @Min(0) Integer page,
                                                                @RequestParam @Min(1) @Max(50) Integer limit) {
        var posts = postService.getPostsByTaskId(taskId, page, limit);
        PageResponseDto<PostResponseDto> responseDto = pageMapper.toPageResponseDto(posts);
        responseDto.setContent(posts.getContent().stream().map(postMapper::postToPostResponseDto).toList());

        return responseDto;
    }

    @DeleteMapping("/{id}")
    @Operation(description = "Удаление таска по ID")
    public void deleteSubject(@PathVariable @Schema(example = "1") Integer id) {
        taskService.deleteTask(id);
    }

}

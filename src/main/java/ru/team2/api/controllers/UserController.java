package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.expression.AccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.page.PageResponseDto;
import ru.team2.api.dto.user.UserResponseDto;
import ru.team2.api.mapper.PageMapper;
import ru.team2.api.mapper.UserMapper;
import ru.team2.domain.services.JwtService;
import ru.team2.domain.services.UserService;

@RestController
@RequiredArgsConstructor
@Tag(name = "users", description = "Контроллер для работы с пользователями")
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final JwtService jwtService;
    private final UserMapper userMapper;
    private final PageMapper pageMapper;

    @GetMapping("/info")
    @Operation(description = "Получение пользователя по Email")
    public UserResponseDto getUserByEmail(@RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var userEmail = jwtService.getEmailFromToken(jwtToken);

        return userMapper.userToUserResponseDto(userService.getUserByEmail(userEmail));
    }

    @GetMapping()
    @Operation(description = "Получение всех пользователей")
    public PageResponseDto<UserResponseDto> getAllUsers(@RequestParam @Min(0) Integer page,
                                                        @RequestParam @Min(1) @Max(50) Integer limit) {
        var users = userService.getAllNotDeletedUsers(page, limit);
        PageResponseDto<UserResponseDto> responseDto = pageMapper.toPageResponseDto(users);
        responseDto.setContent(users.getContent().stream().map(userMapper::userToUserResponseDto).toList());
        return responseDto;
    }

    @PutMapping("{userId}/moderator")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(description = "Выдача роли модератор пользователю")
    public void setModerator(@PathVariable @Min(value = 0, message = "ID не может быть отрицательным") Integer userId) {
        userService.setModerator(userId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(description = "Удаление пользователя по ID")
    public void deleteUserById(@Min(value = 0, message = "ID не может быть отрицательным") @PathVariable @Schema(example = "1") Integer id)
            throws AccessException {
        userService.setUserDeletedById(id);
    }
}

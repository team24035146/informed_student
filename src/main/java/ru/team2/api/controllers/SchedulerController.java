package ru.team2.api.controllers;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.quartz.SchedulerException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.team2.domain.services.QuartzReshedulerService;

@RestController()
@AllArgsConstructor
@RequestMapping("/schedule-job")
public class SchedulerController {

    private QuartzReshedulerService quartzService;

    @PostMapping("/comments")
    public ResponseEntity<?> scheduleJob(@NotNull @RequestParam("frequency-seconds") int seconds) throws SchedulerException {
        quartzService.rescheduleJob(seconds);
        return ResponseEntity.ok().build();
    }
}
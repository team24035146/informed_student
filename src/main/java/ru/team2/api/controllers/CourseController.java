package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.course.CourseResponseDto;
import ru.team2.api.dto.page.PageResponseDto;
import ru.team2.api.dto.subject.SubjectResponseDto;
import ru.team2.api.mapper.CourseMapper;
import ru.team2.api.mapper.PageMapper;
import ru.team2.api.mapper.SubjectMapper;
import ru.team2.domain.services.CourseService;
import ru.team2.domain.services.SubjectService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "courses", description = "Контроллер для работы с курсами университета")
@RequestMapping("/courses")
public class CourseController {
    private final CourseService courseService;
    private final SubjectService subjectService;
    private final CourseMapper courseMapper;
    private final SubjectMapper subjectMapper;
    private final PageMapper pageMapper;

    @GetMapping()
    @Operation(description = "Получение всех курсов")
    public List<CourseResponseDto> getAllCourses() {
        var courses = courseService.getCourses();

        return courseMapper.coursesListToCoursesResponseDtoList(courses);
    }

    @GetMapping("/{courseId}/subjects")
    @Operation(description = "Получение всех предметов курса по ID курса")
    public PageResponseDto<SubjectResponseDto> getAllSubjectsByCourseId(@PathVariable @Schema(example = "1") Integer courseId,
                                                                        @RequestParam @Min(0) Integer page,
                                                                        @RequestParam @Min(1) @Max(50) Integer limit) {
        var subjects = subjectService.getSubjectsByCourseId(courseId, page, limit);
        PageResponseDto<SubjectResponseDto> responseDto = pageMapper.toPageResponseDto(subjects);
        responseDto.setContent(subjects.getContent().stream().map(subjectMapper::subjectToSubjectResponseDto).toList());

        return responseDto;
    }
}

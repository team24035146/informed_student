package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.jwt.JwtEmailResponseDto;
import ru.team2.api.dto.jwt.JwtRoleResponseDto;
import ru.team2.domain.services.JwtService;

import java.util.Collections;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Tag(name = "jwt", description = "Контроллер для работы с Bearer JWT-токенами")
@RequestMapping("/jwt")
public class JwtBearerController {
    private final JwtService jwtService;

    @GetMapping("/email")
    @Operation(description = "Получение email пользователя по JWT-токену")
    public JwtEmailResponseDto getEmailByJwt(@RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var email = jwtService.getEmailFromToken(jwtToken);

        return new JwtEmailResponseDto(email);
    }

    @GetMapping("/role")
    @Operation
    public JwtRoleResponseDto getRoleByJwt(@RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var role = jwtService.getRoleFromToken(jwtToken);

        return new JwtRoleResponseDto(role);
    }
}

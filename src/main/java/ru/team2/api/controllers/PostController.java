package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.comment.CommentResponseDto;
import ru.team2.api.dto.page.PageResponseDto;
import ru.team2.api.dto.post.PostCreateDto;
import ru.team2.api.dto.post.PostResponseDto;
import ru.team2.api.mapper.CommentMapper;
import ru.team2.api.mapper.PageMapper;
import ru.team2.api.mapper.PostMapper;
import ru.team2.domain.entities.User;
import ru.team2.domain.entities.enums.PostType;
import ru.team2.domain.services.*;

@RestController
@RequiredArgsConstructor
@Tag(name = "posts", description = "Контроллер для работы с постами")
@RequestMapping("/posts")
public class PostController {
    private final PostService postService;
    private final CommentService commentService;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final FileService fileService;
    private final JwtService jwtService;
    private final PageMapper pageMapper;

    @GetMapping("/{id}")
    @Operation(description = "Получение поста по ID")
    public PostResponseDto getPostById(@PathVariable @Schema(example = "1") Integer id) {
        var post = postService.getPostByIdAndType(id, PostType.POST);

        return postMapper.postToPostResponseDto(post);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Добавление поста")
    public PostResponseDto addPost(@RequestBody @Valid PostCreateDto postCreateDto,
                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var authorEmail = jwtService.getEmailFromToken(jwtToken);
        var post = postMapper.postCreateDtoToPost(postCreateDto);

        post.setUser(new User());
        post.getUser().setEmail(authorEmail);

        var createdPost = postService.savePost(post, PostType.POST);
        if (postCreateDto.getFileId() != null) {
            fileService.setPost(postCreateDto.getFileId(), createdPost);
        }

        return postMapper.postToPostResponseDto(createdPost);
    }

    @GetMapping("/{postId}/comments")
    @Operation(description = "Получение всех комментариев по ID поста")
    public PageResponseDto<CommentResponseDto> getAllCommentsByPostId(@PathVariable @Schema(example = "1") Integer postId,
                                                                      @RequestParam @Min(0) Integer page,
                                                                      @RequestParam @Min(1) @Max(50) Integer limit) {
        var comments = commentService.getCommentsByPostId(postId, PostType.POST, page, limit);
        PageResponseDto<CommentResponseDto> responseDto = pageMapper.toPageResponseDto(comments);
        responseDto.setContent(comments.getContent().stream().map(commentMapper::commentToCommentResponseDto).toList());
        
        return responseDto;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(description = "Удаление поста по ID")
    public void deletePost(@PathVariable @Schema(example = "1") Integer id) {
        postService.deletePost(id, PostType.POST);
    }
}

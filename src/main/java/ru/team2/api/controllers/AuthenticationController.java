package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.auth.AuthenticationLoginDto;
import ru.team2.api.dto.auth.AuthenticationRegisterDto;
import ru.team2.api.dto.auth.AuthenticationResponseDto;
import ru.team2.api.mapper.AuthenticationMapper;
import ru.team2.domain.services.AuthenticationService;

@RestController
@RequiredArgsConstructor
@Tag(name = "auth", description = "Контроллер для работы с авторизацией")
@RequestMapping("/auth")
public class AuthenticationController {
    private final AuthenticationService authenticationService;
    private final AuthenticationMapper authenticationMapper;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Регистрация пользователя")
    public AuthenticationResponseDto register(@RequestBody AuthenticationRegisterDto authenticationRegisterDto) {
        var user = authenticationMapper.authenticationRegisterDtoToUser(authenticationRegisterDto);

        return new AuthenticationResponseDto(authenticationService.register(user));
    }

    @PostMapping("/authenticate")
    @Operation(description = "Авторизация пользователя")
    public AuthenticationResponseDto authenticate(@RequestBody AuthenticationLoginDto authenticationLoginDto) {
        var user = authenticationMapper.authenticationLoginDtoToUser(authenticationLoginDto);

        return new AuthenticationResponseDto(authenticationService.authenticate(user));
    }
}

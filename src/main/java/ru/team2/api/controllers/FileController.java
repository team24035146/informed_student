package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.team2.api.dto.file.FileResponseDto;
import ru.team2.api.mapper.FileMapper;
import ru.team2.domain.entities.Post;
import ru.team2.domain.services.FileService;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@Tag(name = "files", description = "Контроллер для работы с файлами")
@RequestMapping("/files")
public class FileController {
    final FileMapper fileMapper;
    final FileService fileService;

    @GetMapping()
    @Operation(description = "Получение файла по id поста")
    public FileResponseDto getFileByPost(@RequestParam Integer postId) {
        var file = fileService.getFileByPost(new Post().setId(postId));
        String fileDownloadUri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/files/")
                .path(String.valueOf(file.getId()))
                .toUriString();

        return fileMapper.fileToFileResponseDto(file, fileDownloadUri);
    }

    @GetMapping("/{id}")
    @Operation(description = "Получение файла по id")
    public ResponseEntity<byte[]> getFile(@PathVariable Integer id) {
        var file = fileService.getFile(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .body(file.getFile());
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Создание файла")
    public Integer createFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        var file = fileService.store(multipartFile);
        return file.getId();
    }
}

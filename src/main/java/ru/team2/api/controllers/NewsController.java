package ru.team2.api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team2.api.dto.comment.CommentResponseDto;
import ru.team2.api.dto.page.PageResponseDto;
import ru.team2.api.dto.post.NewsPostCreateDto;
import ru.team2.api.dto.post.PostCreateDto;
import ru.team2.api.dto.post.PostResponseDto;
import ru.team2.api.mapper.CommentMapper;
import ru.team2.api.mapper.PageMapper;
import ru.team2.api.mapper.PostMapper;
import ru.team2.domain.entities.User;
import ru.team2.domain.entities.enums.PostType;
import ru.team2.domain.services.*;

@RestController
@RequiredArgsConstructor
@Tag(name = "news", description = "Контроллер для работы с новостями")
@RequestMapping("/news")
public class NewsController {
    private final PostService postService;
    private final CommentService commentService;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final FileService fileService;
    private final JwtService jwtService;
    private final PageMapper pageMapper;

    @GetMapping()
    @Operation(description = "Получение поста-новости по ID")
    public PageResponseDto<PostResponseDto> getAllNews(@RequestParam @Min(0) Integer page,
                                      @RequestParam @Min(1) @Max(50) Integer limit) {
        var news = postService.getAllNews(page, limit);
        PageResponseDto<PostResponseDto> responseDto = pageMapper.toPageResponseDto(news);
        responseDto.setContent(news.getContent().stream().map(postMapper::postToPostResponseDto).toList());

        return responseDto;
    }

    @GetMapping("/{id}")
    @Operation(description = "Получение поста-новости по ID")
    public PostResponseDto getNewsPostById(@PathVariable @Schema(example = "1") Integer id) {
        var newsPost = postService.getPostByIdAndType(id, PostType.NEWS);

        return postMapper.postToPostResponseDto(newsPost);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Добавление поста-новости")
    public PostResponseDto addNewsPost(@RequestBody @Valid NewsPostCreateDto newsPostCreateDto,
                                       @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        var jwtToken = bearerToken.substring("Bearer ".length());
        var authorEmail = jwtService.getEmailFromToken(jwtToken);
        var newsPost = postMapper.newsPostCreateDtoToPost(newsPostCreateDto);

        newsPost.setUser(new User());
        newsPost.getUser().setEmail(authorEmail);

        var createdPost = postService.savePost(newsPost, PostType.NEWS);

        if (newsPostCreateDto.getFileId() != null) {
            fileService.setPost(newsPostCreateDto.getFileId(), createdPost);
        }

        return postMapper.postToPostResponseDto(createdPost);
    }

    @GetMapping("/{newsPostId}/comments")
    @Operation(description = "Получение всех комментариев по ID поста-новости")
    public PageResponseDto<CommentResponseDto> getAllCommentsByNewsPostId(@PathVariable @Schema(example = "1") Integer newsPostId,
                                                                          @RequestParam @Min(0) Integer page,
                                                                          @RequestParam @Min(1) @Max(50) Integer limit) {
        var comments = commentService.getCommentsByPostId(newsPostId, PostType.NEWS, page, limit);
        PageResponseDto<CommentResponseDto> responseDto = pageMapper.toPageResponseDto(comments);
        responseDto.setContent(comments.getContent().stream().map(commentMapper::commentToCommentResponseDto).toList());

        return responseDto;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(description = "Удаление поста-новости по ID")
    public void deletePost(@PathVariable @Schema(example = "1") Integer id) {
        postService.deletePost(id, PostType.NEWS);
    }
}

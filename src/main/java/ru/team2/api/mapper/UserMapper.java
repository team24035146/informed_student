package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import ru.team2.api.dto.subject.SubjectResponseDto;
import ru.team2.api.dto.user.UserResponseDto;
import ru.team2.domain.entities.Subject;
import ru.team2.domain.entities.User;

import java.util.List;

@Mapper(componentModel = "spring", uses = {DateTimeMapper.class})
public interface UserMapper {
    UserResponseDto userToUserResponseDto(User user);
    List<UserResponseDto> usersListToUsersResponseDtoList(List<User> users);
}

package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.team2.api.dto.auth.AuthenticationLoginDto;
import ru.team2.api.dto.auth.AuthenticationRegisterDto;
import ru.team2.domain.entities.User;

@Mapper(componentModel = "spring")
public interface AuthenticationMapper {
    User authenticationLoginDtoToUser(AuthenticationLoginDto authenticationLoginDto);
    User authenticationRegisterDtoToUser(AuthenticationRegisterDto authenticationRegisterDto);
}

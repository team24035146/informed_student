package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.team2.api.dto.subject.SubjectCreateDto;
import ru.team2.api.dto.subject.SubjectResponseDto;
import ru.team2.domain.entities.Subject;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SubjectMapper {
    SubjectResponseDto subjectToSubjectResponseDto(Subject subject);

    @Mapping(target = "course.id", source = "subjectCreateDto.courseId")
    Subject subjectCreateDtoToSubject(SubjectCreateDto subjectCreateDto);

    List<SubjectResponseDto> subjectsListToSubjectsResponseDtoList(List<Subject> subjects);
}

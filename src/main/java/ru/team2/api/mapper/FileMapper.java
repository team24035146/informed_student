package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.team2.api.dto.file.FileResponseDto;
import ru.team2.domain.entities.File;

@Mapper(componentModel = "spring")
public interface FileMapper {

    @Mapping(target = "url", source = "url")
    FileResponseDto fileToFileResponseDto(File file, String url);
}

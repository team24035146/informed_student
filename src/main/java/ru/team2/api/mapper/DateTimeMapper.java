package ru.team2.api.mapper;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class DateTimeMapper {
    public String dateTimeToStringMapperMethod(LocalDateTime localDateTime) {
        var format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (localDateTime == null) return null;
        return localDateTime.format(format);
    }
}

package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import ru.team2.api.dto.comment.CommentCreateDto;
import ru.team2.api.dto.comment.CommentResponseDto;
import ru.team2.api.dto.comment.CommentUpdateDto;
import ru.team2.api.dto.comment.ThreadCommentCreateDto;
import ru.team2.domain.entities.Comment;

import java.util.List;

@Mapper(componentModel = "spring", uses = {DateTimeMapper.class})
public interface CommentMapper {
    @Mapping(target = "authorName", source = "comment", qualifiedByName = "GetCommentAuthor")
    CommentResponseDto commentToCommentResponseDto(Comment comment);

    @Mapping(target = "post.id", source = "commentCreateDto.postId")
    Comment commentCreateDtoToComment(CommentCreateDto commentCreateDto);

    @Mapping(target = "post.id", source = "commentUpdateDto.postId")
    Comment commentUpdateDtoToComment(CommentUpdateDto commentUpdateDto);

    @Mapping(target = "parentComment.id", source = "threadCommentCreateDto.commentId")
    Comment threadCommentCreateDtoToComment(ThreadCommentCreateDto threadCommentCreateDto);

    List<CommentResponseDto> commentsListToCommentsResponseDtoList(List<Comment> comments);

    @Named("GetCommentAuthor")
    default String getCommentAuthor(Comment comment) {
        if (comment.isAnonymous() == null || comment.isAnonymous()) {
            return null;
        }

        return comment.getUser().isDeleted() ? "[DELETED_USER]" : comment.getUser().getName();
    }
}

package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.team2.api.dto.page.PageResponseDto;

@Service
public class PageMapper {
    public <T, V> PageResponseDto<V> toPageResponseDto(Page<T> page) {
        var pageResponseDto = new PageResponseDto<V>();
        pageResponseDto.setTotalPages(page.getTotalPages());
        pageResponseDto.setTotalElements(page.getTotalElements());
        pageResponseDto.setPageNumber(page.getNumber());
        pageResponseDto.setPageLimit(page.getSize());
        return pageResponseDto;
    }
}

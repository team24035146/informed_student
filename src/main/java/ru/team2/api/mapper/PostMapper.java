package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import ru.team2.api.dto.post.NewsPostCreateDto;
import ru.team2.api.dto.post.PostCreateDto;
import ru.team2.api.dto.post.PostResponseDto;
import ru.team2.api.dto.subject.SubjectResponseDto;
import ru.team2.api.dto.task.TaskResponseDto;
import ru.team2.domain.entities.Post;
import ru.team2.domain.entities.Subject;
import ru.team2.domain.entities.Task;
import ru.team2.domain.entities.User;

import java.util.List;

@Mapper(componentModel = "spring", uses = {DateTimeMapper.class})
public interface PostMapper {
    @Mapping(target = "authorName", source = "post.user", qualifiedByName = "MapPostAuthor")
    PostResponseDto postToPostResponseDto(Post post);

    @Mapping(target = "task.id", source = "postCreateDto.taskId")
    Post postCreateDtoToPost(PostCreateDto postCreateDto);

    Post newsPostCreateDtoToPost(NewsPostCreateDto newsPostCreateDto);

    List<PostResponseDto> postsListToPostsResponseDtoList(List<Post> posts);

    @Named("MapPostAuthor")
    default String mapPostAuthor(User user) {
        return user.isDeleted() ? "[DELETED_USER]" : user.getName();
    }
}

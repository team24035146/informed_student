package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import ru.team2.api.dto.course.CourseResponseDto;
import ru.team2.domain.entities.Course;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourseMapper {
    CourseResponseDto courseToCourseResponseDto(Course course);

    List<CourseResponseDto> coursesListToCoursesResponseDtoList(List<Course> courses);
}

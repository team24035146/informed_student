package ru.team2.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.team2.api.dto.subject.SubjectCreateDto;
import ru.team2.api.dto.subject.SubjectResponseDto;
import ru.team2.api.dto.task.TaskCreateDto;
import ru.team2.api.dto.task.TaskResponseDto;
import ru.team2.domain.entities.Subject;
import ru.team2.domain.entities.Task;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    TaskResponseDto taskToTaskResponseDto(Task task);

    @Mapping(target = "subject.id", source = "taskCreateDto.subjectId")
    Task taskCreateDtoToTask(TaskCreateDto taskCreateDto);

    List<TaskResponseDto> tasksListToTasksResponseDtoList(List<Task> tasks);
}

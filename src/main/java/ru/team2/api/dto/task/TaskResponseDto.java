package ru.team2.api.dto.task;

import lombok.Data;

@Data
public class TaskResponseDto {
    private Integer id;
    private String categoryType;
}

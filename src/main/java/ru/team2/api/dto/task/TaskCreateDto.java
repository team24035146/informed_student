package ru.team2.api.dto.task;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.team2.api.validation.constrains.CategoryTypeConstraint;

@Data
@Accessors(chain = true)
public class TaskCreateDto {
    @Min(value = 0, message = "ID предмета не может быть отрицательным")
    @NotNull(message = "ID предмета не может быть null")
    @Schema(name = "subjectId", example = "1")
    @JsonAlias("subject_id")
    private Integer subjectId;

    @Size(max = 255, message = "Слишком длинное название типа таска")
    @CategoryTypeConstraint
    @NotBlank(message = "Не указан тип таска")
    @Schema(name = "categoryType", example = "TEST")
    @JsonAlias("category_type")
    private String categoryType;
}

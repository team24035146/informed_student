package ru.team2.api.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
public class AuthenticationResponseDto {
    private String jwtToken;
}

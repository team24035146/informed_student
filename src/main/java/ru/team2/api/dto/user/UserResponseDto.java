package ru.team2.api.dto.user;

import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;
import ru.team2.domain.entities.enums.UserRole;

import java.time.LocalDateTime;

@Data
public class UserResponseDto {
    private Integer id;
    private String name;
    private String email;
    private String role;
    private String registrationTime;
}

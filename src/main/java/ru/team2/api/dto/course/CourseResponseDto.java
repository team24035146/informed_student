package ru.team2.api.dto.course;

import lombok.Data;

@Data
public class CourseResponseDto {
    private Integer id;
    private String name;
}

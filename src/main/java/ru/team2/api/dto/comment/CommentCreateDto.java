package ru.team2.api.dto.comment;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class CommentCreateDto {
    @Min(value = 0, message = "ID поста не может быть отрицательным")
    @NotNull(message = "ID поста не может быть null")
    @Schema(name = "postId", example = "1")
    @JsonAlias("post_id")
    private Integer postId;

    @Size(max = 5000, message = "Слишком длинный текст комментария")
    @NotBlank(message = "У комментария не может быть пустой текст")
    @Schema(name = "content", example = "Hello, world!")
    private String content;

    @Schema(name = "isAnonymous", example = "false")
    private Boolean isAnonymous;
}

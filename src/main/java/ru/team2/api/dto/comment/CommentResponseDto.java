package ru.team2.api.dto.comment;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CommentResponseDto {
    private Integer id;
    private String createdAt;
    private String updatedAt;
    private String content;
    private String authorName;
    private Boolean isAnonymous;
}

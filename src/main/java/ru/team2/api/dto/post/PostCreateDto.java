package ru.team2.api.dto.post;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class PostCreateDto {
    @Min(value = 0, message = "ID таска не может быть отрицательным")
    @NotNull(message = "ID таска не может быть null")
    @Schema(name = "taskId", example = "1")
    @JsonAlias("task_id")
    private Integer taskId;

    @Size(max = 5000, message = "Слишком длинное название поста")
    @NotBlank(message = "У поста не может быть пустое название")
    @Schema(name = "name", example = "DefaultName")
    private String name;

    @Size(max = 5000, message = "Слишком длинный текст поста")
    @NotBlank(message = "У поста не может быть пустой текст")
    @Schema(name = "content", example = "Hello, world!")
    private String content;

    @Min(value = 0, message = "ID файла не может быть отрицательным")
    @Schema(name = "fileId", example = "1")
    @JsonAlias("file_id")
    private Integer fileId;
}

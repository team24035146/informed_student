package ru.team2.api.dto.post;

import lombok.Data;

@Data
public class PostResponseDto {
    private Integer id;
    private String name;
    private String createdAt;
    private String updatedAt;
    private String content;
    private String authorName;
}

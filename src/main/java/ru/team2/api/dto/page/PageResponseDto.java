package ru.team2.api.dto.page;

import lombok.Data;

import java.util.List;

@Data
public class PageResponseDto<T> {
    private int totalPages;
    private long totalElements;
    private int pageNumber;
    private int pageLimit;
    private List<T> content;
}

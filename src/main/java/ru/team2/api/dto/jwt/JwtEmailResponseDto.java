package ru.team2.api.dto.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class JwtEmailResponseDto {
    private String email;
}

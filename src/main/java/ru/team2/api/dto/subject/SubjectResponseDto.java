package ru.team2.api.dto.subject;

import lombok.Data;

@Data
public class SubjectResponseDto {
    private Integer id;
    private String name;
}

package ru.team2.api.dto.subject;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SubjectCreateDto {
    @Min(value = 0, message = "ID курса не может быть отрицательным")
    @NotNull(message = "ID курса не может быть null")
    @Schema(name = "courseId", example = "1")
    @JsonAlias("course_id")
    Integer courseId;

    @Size(max = 255, message = "Слишком длинное название предмета")
    @NotBlank(message = "Не указано название предмета")
    @Schema(name = "name", example = "DefaultSubjectName")
    String name;
}

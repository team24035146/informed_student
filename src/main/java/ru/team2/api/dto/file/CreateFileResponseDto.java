package ru.team2.api.dto.file;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateFileResponseDto {
    public Integer id;

    public String message;
}

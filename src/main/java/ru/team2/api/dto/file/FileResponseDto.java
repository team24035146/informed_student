package ru.team2.api.dto.file;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FileResponseDto {
    public String name;

    public String url;

    public String type;
}

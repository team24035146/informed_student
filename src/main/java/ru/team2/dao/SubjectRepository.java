package ru.team2.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.team2.domain.entities.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
    Page<Subject> findAllByCourseId(Integer courseId, Pageable pageable);
}

package ru.team2.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.team2.domain.entities.Comment;

import java.time.LocalDateTime;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Page<Comment> findAllByPostIdAndParentCommentIsNull(Integer postId, Pageable pageable);
    Page<Comment> findAllByParentCommentId(Integer parentCommentId, Pageable pageable);
    void deleteByCreatedAtBefore(LocalDateTime time);
}

package ru.team2.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.team2.domain.entities.Post;
import ru.team2.domain.entities.enums.PostType;

import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Integer> {
    Page<Post> findAllByTaskId(Integer taskId, Pageable pageable);
    Optional<Post> findByIdAndType(Integer id, PostType type);
    Page<Post> findAllByType(PostType type, Pageable pageable);
}

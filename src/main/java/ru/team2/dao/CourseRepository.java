package ru.team2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team2.domain.entities.Course;

public interface CourseRepository extends JpaRepository<Course, Integer> {
}

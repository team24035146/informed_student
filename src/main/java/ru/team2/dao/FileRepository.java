package ru.team2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team2.domain.entities.File;

public interface FileRepository extends JpaRepository<File, Integer> {
}

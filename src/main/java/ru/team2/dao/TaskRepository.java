package ru.team2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team2.domain.entities.Task;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    List<Task> findAllBySubjectId(Integer subjectId);
}

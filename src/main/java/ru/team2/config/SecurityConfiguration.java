package ru.team2.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import ru.team2.auth.JwtAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final AuthenticationProvider authenticationProvider;
    private final LogoutHandler logoutHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .cors(Customizer.withDefaults())
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(request -> request
                            .requestMatchers("/auth/**").permitAll()
                            .requestMatchers("/swagger-ui/**").permitAll()
                            .requestMatchers("/v3/api-docs/**").permitAll()
                            .requestMatchers(HttpMethod.GET, "/files/**").permitAll()
                            .requestMatchers("/schedule-job/**").hasRole("ADMIN")
                            .requestMatchers("/comments/*/revisions").hasRole("ADMIN")
                            .requestMatchers(HttpMethod.DELETE, "/posts/**").hasRole("ADMIN")
                            .requestMatchers(HttpMethod.POST, "/subjects").hasRole("ADMIN")
                            .requestMatchers(HttpMethod.DELETE, "/subjects/**").hasRole("ADMIN")
                            .requestMatchers(HttpMethod.DELETE, "/tasks/**").hasRole("ADMIN")
                            .requestMatchers(HttpMethod.POST, "/news").hasAnyRole("ADMIN", "MODERATOR")
                            .requestMatchers(HttpMethod.DELETE, "/news/**").hasAnyRole("ADMIN", "MODERATOR")
                            .requestMatchers(HttpMethod.DELETE, "/comments/**").hasAnyRole("ADMIN", "MODERATOR")
                            .requestMatchers(HttpMethod.PUT, "/users/moderator").hasRole("ADMIN")
                            .requestMatchers(HttpMethod.DELETE, "/users/**").hasRole("ADMIN")
                            .anyRequest()
                            .authenticated())
                .sessionManagement(configurer -> configurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .logout(logout -> logout.logoutUrl("/auth/logout")
                                .addLogoutHandler(logoutHandler)
                                .logoutSuccessHandler((request, response, authentication) -> SecurityContextHolder.clearContext())
                );

        return httpSecurity.build();
    }
}

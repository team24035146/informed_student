package ru.team2.sheduler;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.team2.domain.services.implementation.CommentServiceImpl;

@Component
@Slf4j
public class DeleteCommentsJob implements Job {

    @Value("${application.lifetime-seconds.comment}")
    private int COMMENT_LIFETIME_SEC;

    @Autowired
    private CommentServiceImpl commentService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        commentService.deleteByCreatedAtBefore(COMMENT_LIFETIME_SEC);
        log.info("DELETE OLD COMMENTS");
    }
}
package ru.team2.domain.services;

import org.springframework.web.multipart.MultipartFile;
import ru.team2.domain.entities.File;
import ru.team2.domain.entities.Post;

import java.io.IOException;

public interface FileService {
    void setPost(Integer fileId, Post post);

    File getFile(Integer id);

    File getFileByPost(Post post);

    File store(MultipartFile multipartFile) throws IOException;
}

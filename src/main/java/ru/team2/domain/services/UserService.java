package ru.team2.domain.services;

import org.springframework.data.domain.Page;
import org.springframework.expression.AccessException;
import ru.team2.domain.entities.User;

import java.util.List;

public interface UserService {
    Page<User> getAllNotDeletedUsers(Integer pageNumber, Integer limit);
    User getUserById(Integer userId);
    User getUserByName(String name);
    User getUserByEmail(String email);
    void setModerator(Integer userId);
    void setUserDeletedById(Integer id) throws AccessException;
}

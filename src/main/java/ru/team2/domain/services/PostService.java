package ru.team2.domain.services;

import org.springframework.data.domain.Page;
import ru.team2.domain.entities.Post;
import ru.team2.domain.entities.enums.PostType;

public interface PostService {
    Post getPost(Integer id);
    Post getPostByIdAndType(Integer id, PostType type);
    Post savePost(Post post, PostType type);
    void deletePost(Integer id, PostType type);
    Page<Post> getPostsByTaskId(Integer taskId, Integer page, Integer limit);
    Page<Post> getAllNews(Integer page, Integer limit);
}

package ru.team2.domain.services;

import org.quartz.SchedulerException;

public interface QuartzReshedulerService {
    void rescheduleJob(int frequency) throws SchedulerException;
}

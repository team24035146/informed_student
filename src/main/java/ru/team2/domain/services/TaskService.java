package ru.team2.domain.services;

import ru.team2.domain.entities.Subject;
import ru.team2.domain.entities.Task;

import java.util.List;

public interface TaskService {
    Task getTask(Integer id);
    List<Task> getTasksBySubject(Subject subject);
    List<Task> getTasksBySubjectId(Integer subjectId);
    Task saveTask(Task task);
    Task updateTask(Task task);
    Task deleteTask(Integer id);
    List<String> getAllCategoryTypes();
}

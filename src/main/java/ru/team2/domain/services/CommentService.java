package ru.team2.domain.services;

import org.springframework.data.domain.Page;
import org.springframework.expression.AccessException;
import ru.team2.domain.entities.Comment;
import ru.team2.domain.entities.enums.PostType;

import java.util.List;

public interface CommentService {
    Comment getComment(Integer id);

    Comment saveComment(Comment comment);
    Comment saveThreadComment(Comment comment) throws AccessException;
    Comment updateComment(Comment comment);
    void deleteComment(Integer id);
    Page<Comment> getCommentsByPostId(Integer postId, PostType postType, Integer pageNumber, Integer limit);
    Page<Comment> getThreadByCommentId(Integer commentId, Integer pageNumber, Integer limit);
    void deleteByCreatedAtBefore(int COMMENT_LIFETIME_SEC);

    List<Comment> getRevisions(Integer id);
}

package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.team2.dao.SubjectRepository;
import ru.team2.domain.entities.Course;
import ru.team2.domain.entities.Subject;
import ru.team2.api.exceptions.SubjectNotFoundException;
import ru.team2.domain.services.SubjectService;
import ru.team2.domain.services.CourseService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;
    private final CourseService courseService;

    public Subject getSubject(Integer id) {
        return subjectRepository.findById(id)
                .orElseThrow(() -> new SubjectNotFoundException("Не найден предмет с id " + id));
    }

    public Subject getSubject(String subjectName) {
        var subjectExample = Example.of(new Subject().setName(subjectName));
        return subjectRepository.findOne(subjectExample)
                .orElseThrow(() -> new SubjectNotFoundException("Не найден предмет с названием " + subjectName));
    }

    public List<Subject> getSubjectsByCourse(Course course) {
        var subjectExample = Example.of(new Subject().setCourse(course));
        return subjectRepository.findAll(subjectExample);
    }

    @Override
    public Page<Subject> getSubjectsByCourseId(Integer courseId, Integer page, Integer limit) {
        var pageable = PageRequest.of(page, limit, Sort.by(Sort.Order.desc("id")));
        return subjectRepository.findAllByCourseId(courseId, pageable);
    }

    public Subject saveSubject(Subject subject) {
        courseService.getCourse(subject.getCourse().getId());
        return subjectRepository.save(subject);
    }

    public Subject updateSubject(Subject subject) {
        getSubject(subject.getId());
        courseService.getCourse(subject.getCourse().getId());
        return subjectRepository.save(subject);
    }

    public Subject deleteSubject(Integer id) {
        var subjectToDelete = getSubject(id);
        subjectRepository.deleteById(id);
        return subjectToDelete;
    }
}

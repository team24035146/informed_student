package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import ru.team2.domain.services.QuartzReshedulerService;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuartzReshedulerServiceImpl implements QuartzReshedulerService {
    private final SchedulerFactoryBean schedulerFactory;

    public void rescheduleJob(int newFrequency){

        Scheduler scheduler = schedulerFactory.getScheduler();
        TriggerKey triggerKey = new TriggerKey("deleteCommentsTrigger", "commentsGroup");

        Trigger newTrigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(newFrequency)
                        .repeatForever())
                .build();
        try {
            scheduler.rescheduleJob(triggerKey, newTrigger);
            log.info("RESCHEDULED JOB");
        } catch (SchedulerException e) {
            log.error("ERROR IN SCHEDULER");
        }
    }
}

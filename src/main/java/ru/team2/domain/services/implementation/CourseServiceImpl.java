package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import ru.team2.dao.CourseRepository;
import ru.team2.domain.entities.Course;
import ru.team2.api.exceptions.CourseNotFoundException;
import ru.team2.domain.services.CourseService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;

    public Course getCourse(Integer id) {
        return courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException("Не найден курс с id " + id));
    }

    public Course getCourse(String courseName) {
        var courseExample = Example.of(new Course().setName(courseName));
        return courseRepository.findOne(courseExample).orElseThrow(() -> new CourseNotFoundException("Не найден курс с названием " + courseName));
    }

    public List<Course> getCourses() {
        return courseRepository.findAll();
    }

    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }
}

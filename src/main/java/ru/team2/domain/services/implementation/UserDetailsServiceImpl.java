package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.team2.api.exceptions.UserNotFoundException;
import ru.team2.dao.UserRepository;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository
                .findByEmail(username)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с указанным Email не найден"));

        if (user.isDeleted()) {
            throw new UserNotFoundException("Пользователь с указанным Email удален");
        }

        return user;
    }
}

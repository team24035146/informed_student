package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.team2.api.exceptions.FileNotFoundException;
import ru.team2.dao.FileRepository;
import ru.team2.domain.entities.File;
import ru.team2.domain.entities.Post;
import ru.team2.domain.services.FileService;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    final FileRepository fileRepository;

    @Override
    public void setPost(Integer fileId, Post post) {
        var file = fileRepository.findById(fileId).orElseThrow(() -> new FileNotFoundException("Не найден файл с id: " + fileId));
        file.setPost(post);
        fileRepository.save(file);
    }

    @Override
    public File getFile(Integer id) {
        var file = fileRepository.findById(id).orElseThrow(() -> new FileNotFoundException("Не найден файл с id " + id));
        file.setPost(null);
        return file;
    }

    @Override
    public File getFileByPost(Post post) {
        var fileExample = Example.of(new File().setPost(post));
        var file = fileRepository.findOne(fileExample).orElseThrow(() -> new FileNotFoundException("Не найден файл у поста с id: " + post.getId()));
        file.setPost(null);
        return file;
    }

    @Override
    public File store(MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        var file = new File().setName(fileName).setType(multipartFile.getContentType()).setFile(multipartFile.getBytes());

        return fileRepository.save(file);
    }
}

package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.team2.api.exceptions.PostNotFoundException;
import ru.team2.dao.PostRepository;
import ru.team2.domain.entities.Post;
import ru.team2.domain.entities.enums.PostType;
import ru.team2.domain.services.PostService;
import ru.team2.domain.services.TaskService;
import ru.team2.domain.services.UserService;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final UserService userService;
    private final TaskService taskService;


    public Post getPost(Integer id) {
        return postRepository.findById(id)
                .orElseThrow(() -> new PostNotFoundException("Не найден пост с id " + id));
    }

    public Post getPostByIdAndType(Integer id, PostType type) {
        return postRepository.findByIdAndType(id, type)
                .orElseThrow(() -> new PostNotFoundException("Не найден пост с id " + id + " и типом " + type));
    }

    public Post savePost(Post post, PostType type) {
        if (post.getTask() != null) {
            taskService.getTask(post.getTask().getId());
        }
        var author = userService.getUserByEmail(post.getUser().getUsername());
        post.setUser(author);
        post.setType(type);

        return postRepository.save(post);
    }

    public void deletePost(Integer id, PostType type) {
        getPostByIdAndType(id, type);
        postRepository.deleteById(id);
    }

    public Page<Post> getPostsByTaskId(Integer taskId, Integer page, Integer limit) {
        var pageable = PageRequest.of(page, limit, Sort.by(Sort.Order.desc("createdAt")));

        return postRepository.findAllByTaskId(taskId, pageable);
    }

    @Override
    public Page<Post> getAllNews(Integer page, Integer limit) {
        var pageable = PageRequest.of(page, limit, Sort.by(Sort.Order.desc("createdAt")));

        return postRepository.findAllByType(PostType.NEWS, pageable);
    }
}



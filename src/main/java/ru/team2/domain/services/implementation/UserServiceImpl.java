package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.expression.AccessException;
import org.springframework.stereotype.Service;
import ru.team2.api.exceptions.BadRequestException;
import ru.team2.api.exceptions.UserNotFoundException;
import ru.team2.dao.UserRepository;
import ru.team2.domain.entities.User;
import ru.team2.domain.entities.enums.UserRole;
import ru.team2.domain.services.UserService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public Page<User> getAllNotDeletedUsers(Integer pageNumber, Integer limit) {
        var pageable = PageRequest.of(pageNumber, limit, Sort.by(Sort.Order.asc("id")));
        return userRepository.findAllByIsDeletedFalse(pageable);
    }

    @Override
    public User getUserById(Integer userId) {
        var user = userRepository
                .findById(userId)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с указанным ID не найден"));

        if (user.isDeleted()) {
            user.setName("[DELETED_USER]");
        }

        return user;
    }

    @Override
    public User getUserByName(String name) {
        var user = userRepository
                .findByName(name)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с указанным именем не найден"));

        if (user.isDeleted()) {
            user.setName("[DELETED_USER]");
        }

        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        var user = userRepository
                .findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с указанной почтой не найден"));

        if (user.isDeleted()) {
            user.setName("[DELETED_USER]");
        }

        return user;
    }

    @Override
    public void setModerator(Integer userId) {
        var user = getUserById(userId);

        if (user.isDeleted())
            throw new UserNotFoundException("Пользователь удален");

        if (user.getRole() == UserRole.ADMIN || user.getRole() == UserRole.MODERATOR)
            throw new BadRequestException("Модератором можно назначить только пользователя");

        user.setRole(UserRole.MODERATOR);
        userRepository.save(user);
    }

    @Override
    public void setUserDeletedById(Integer id) throws AccessException {
        var user = getUserById(id);
        if (user.getRole() == UserRole.ADMIN) {
            throw new AccessException("Запрещено удалять админов");
        }
        user.setDeleted(true);
        userRepository.save(user);
    }
}

package ru.team2.domain.services.implementation;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.expression.AccessException;
import org.springframework.stereotype.Service;
import ru.team2.api.exceptions.BadRequestException;
import ru.team2.api.exceptions.CommentNotFoundException;
import ru.team2.dao.CommentRepository;
import ru.team2.domain.entities.Comment;
import ru.team2.domain.entities.enums.PostType;
import ru.team2.domain.services.CommentService;
import ru.team2.domain.services.PostService;
import ru.team2.domain.services.UserService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final UserService userService;
    private final PostService postService;
    private final EntityManager em;

    public Comment getComment(Integer id) {
        return commentRepository.findById(id)
                .orElseThrow(() -> new CommentNotFoundException("Не найден комментарий с id " + id));
    }

    public Comment saveComment(Comment comment) {
        postService.getPost(comment.getPost().getId());
        var author = userService.getUserByEmail(comment.getUser().getUsername());
        comment.setUser(author);

        return commentRepository.save(comment);
    }

    @Override
    public Comment saveThreadComment(Comment comment) throws AccessException {
        var parentComment = getComment(comment.getParentComment().getId());

        if (parentComment.getPost().getType() != PostType.NEWS) {
            throw new BadRequestException("Тред есть только у комменариев в новостях");
        }
        if (parentComment.getParentComment() != null) {
            throw new BadRequestException("У комментариев в треде не может быть своих тредов");
        }

        var author = userService.getUserByEmail(comment.getUser().getUsername());
        comment.setUser(author);
        comment.setPost(parentComment.getPost());

        return commentRepository.save(comment);
    }

    public Comment updateComment(Comment comment) {
        var commentDb = getComment(comment.getId());
        postService.getPost(comment.getPost().getId());
        comment.setCreatedAt(commentDb.getCreatedAt());
        Optional.ofNullable(commentDb.getParentComment())
                .map(parentComment -> new Comment().setId(parentComment.getId()))
                .ifPresentOrElse(comment::setParentComment, () -> comment.setParentComment(null));
        return commentRepository.save(comment);
    }

    public void deleteComment(Integer id) {
        getComment(id);
        commentRepository.deleteById(id);
    }

    @Override
    public Page<Comment> getCommentsByPostId(Integer postId, PostType postType, Integer pageNumber, Integer limit) {
        postService.getPostByIdAndType(postId, postType);
        var pageable = PageRequest.of(pageNumber, limit, Sort.by(Sort.Order.desc("createdAt")));
        return commentRepository.findAllByPostIdAndParentCommentIsNull(postId, pageable);
    }

    @Override
    public Page<Comment> getThreadByCommentId(Integer commentId, Integer pageNumber, Integer limit) {
        getComment(commentId);
        var pageable = PageRequest.of(pageNumber, limit, Sort.by(Sort.Order.desc("createdAt")));
        return commentRepository.findAllByParentCommentId(commentId, pageable);
    }

    @Override
    @Transactional
    public void deleteByCreatedAtBefore(int seconds) {
        LocalDateTime thresholdDate = LocalDateTime.now().minusSeconds(seconds);
        commentRepository.deleteByCreatedAtBefore(thresholdDate);
    }

    @Override
    public List<Comment> getRevisions(Integer id) {
        return AuditReaderFactory.get(em).createQuery()
                .forRevisionsOfEntity(Comment.class, true, true)
                .add(AuditEntity.id().eq(id))
                .getResultList();
    }
}

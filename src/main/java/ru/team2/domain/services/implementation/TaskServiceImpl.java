package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import ru.team2.api.exceptions.TaskNotFoundException;
import ru.team2.dao.TaskRepository;
import ru.team2.domain.entities.Subject;
import ru.team2.domain.entities.Task;
import ru.team2.domain.services.TaskService;
import ru.team2.domain.services.SubjectService;
import ru.team2.domain.entities.enums.CategoryType;


import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final SubjectService subjectService;

    public Task getTask(Integer id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException("Не найдена задача с id " + id));
    }

    public List<Task> getTasksBySubject(Subject subject) {
        var TaskExample = Example.of(new Task().setSubject(subject));
        return taskRepository.findAll(TaskExample);
    }

    @Override
    public List<Task> getTasksBySubjectId(Integer subjectId) {
        return taskRepository.findAllBySubjectId(subjectId);
    }

    public Task saveTask(Task task) {
        subjectService.getSubject(task.getSubject().getId());
        return taskRepository.save(task);
    }

    public Task updateTask(Task task) {
        getTask(task.getId());
        subjectService.getSubject(task.getSubject().getId());
        return taskRepository.save(task);
    }

    public Task deleteTask(Integer id) {
        var TaskToDelete = getTask(id);
        taskRepository.deleteById(id);
        return TaskToDelete;
    }

    @Override
    public List<String> getAllCategoryTypes() {
        return Stream.of(CategoryType.values())
                .map(Enum::name)
                .toList();
    }
}

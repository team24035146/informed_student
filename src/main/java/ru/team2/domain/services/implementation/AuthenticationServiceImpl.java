package ru.team2.domain.services.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.team2.dao.UserRepository;
import ru.team2.domain.entities.User;
import ru.team2.domain.entities.enums.UserRole;
import ru.team2.domain.services.AuthenticationService;
import ru.team2.domain.services.JwtService;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;

    @Override
    public String register(User user) {
        encodeUserPassword(user);
        user.setRole(UserRole.USER);
        user.setDeleted(false);
        userRepository.save(user);

        return jwtService.generateToken(user);
    }

    @Override
    public String authenticate(User user) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        user.getUsername(),
                        user.getPassword()
                )
        );
        var userDetails = userDetailsService.loadUserByUsername(user.getUsername());
        return jwtService.generateToken(userDetails);
    }

    private void encodeUserPassword(User user) {
        var userPassword = user.getPassword();
        user.setPassword(passwordEncoder.encode(userPassword));
    }
}

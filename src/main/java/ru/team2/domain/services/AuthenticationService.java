package ru.team2.domain.services;

import ru.team2.domain.entities.User;

public interface AuthenticationService {
    String register(User user);
    String authenticate(User user);
}

package ru.team2.domain.services;

import ru.team2.domain.entities.Course;

import java.util.List;

public interface CourseService {
    Course getCourse(Integer id);
    Course getCourse(String courseName);
    List<Course> getCourses();
    Course saveCourse(Course course);
}

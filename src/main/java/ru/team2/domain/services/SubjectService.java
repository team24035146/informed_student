package ru.team2.domain.services;

import org.springframework.data.domain.Page;
import ru.team2.domain.entities.Course;
import ru.team2.domain.entities.Subject;

import java.util.List;

public interface SubjectService {
    Subject getSubject(Integer id);
    Subject getSubject(String subjectName);
    List<Subject> getSubjectsByCourse(Course course);
    Page<Subject> getSubjectsByCourseId(Integer courseId, Integer page, Integer limit);
    Subject saveSubject(Subject subject);
    Subject updateSubject(Subject subject);
    Subject deleteSubject(Integer id);
}

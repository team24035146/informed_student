package ru.team2.domain.entities.enums;

public enum UserRole {
    USER,
    MODERATOR,
    ADMIN
}

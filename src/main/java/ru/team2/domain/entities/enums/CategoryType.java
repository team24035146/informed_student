package ru.team2.domain.entities.enums;

public enum CategoryType {
    TEST,
    CONSPECT,
    LECTURE,
    EXAM
}

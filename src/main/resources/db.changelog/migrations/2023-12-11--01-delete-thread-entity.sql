BEGIN
TRANSACTION;

DROP TABLE threads;

DROP SEQUENCE threads_id_seq;

ALTER TABLE comments
    ADD COLUMN parent_comment_id INT;

CREATE INDEX idx_comments_parent_comment_id
    ON comments (parent_comment_id);

COMMIT;
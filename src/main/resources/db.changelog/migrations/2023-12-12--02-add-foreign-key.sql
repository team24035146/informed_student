BEGIN
TRANSACTION;

ALTER TABLE comments
    ADD CONSTRAINT parent_comment_fkey FOREIGN KEY (parent_comment_id) REFERENCES comments (id) ON DELETE CASCADE;

COMMIT;
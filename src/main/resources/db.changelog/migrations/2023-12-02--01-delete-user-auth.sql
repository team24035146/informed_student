BEGIN
TRANSACTION;
CREATE TABLE users_migrate
(
    id       INT DEFAULT nextval('user_id_seq') NOT NULL PRIMARY KEY,
    name     TEXT                               NOT NULL,
    email    TEXT                               NOT NULL,
    password TEXT                               NOT NULL
);

INSERT INTO users_migrate (id, name, email, password)
SELECT u.id, u.name, ua.email, ua.password
FROM users u
         JOIN user_auths ua on u.id = ua.user_id;

DROP TABLE user_auths;

DELETE
FROM users;

ALTER TABLE users
    ADD COLUMN  email      TEXT                   NOT NULL UNIQUE,
    DROP COLUMN role,
    ADD COLUMN  role       TEXT    DEFAULT 'USER' NOT NULL,
    ADD COLUMN  password   TEXT                   NOT NULL,
    ADD COLUMN  is_deleted BOOLEAN DEFAULT FALSE  NOT NULL;

INSERT INTO users (id, name, email, password)
SELECT id, name, email, password
FROM users_migrate;

DROP TABLE users_migrate;

COMMIT;

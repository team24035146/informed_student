DELETE FROM subjects;
DELETE FROM tasks;
DELETE FROM posts;
DELETE FROM comments;
DELETE FROM files;

ALTER TABLE subjects DROP COLUMN name;
ALTER TABLE subjects ADD COLUMN name TEXT NOT NULL;
ALTER TABLE subjects ADD CONSTRAINT uc_subject_name UNIQUE (course_id, name);

ALTER TABLE tasks DROP COLUMN title;
ALTER TABLE tasks ADD CONSTRAINT uc_task_type UNIQUE (subject_id, type);

ALTER TABLE posts DROP COLUMN name;
ALTER TABLE posts ADD COLUMN name TEXT NOT NULL;
ALTER TABLE posts ADD CONSTRAINT uc_post_name UNIQUE (task_id, name);
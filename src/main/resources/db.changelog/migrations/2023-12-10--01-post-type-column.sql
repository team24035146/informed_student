BEGIN
TRANSACTION;

CREATE TABLE posts_migrate
(
    id           INT DEFAULT nextval('user_id_seq') NOT NULL PRIMARY KEY,
    user_id      INT                                NOT NULL,
    task_id      INT                                NOT NULL,
    created_at   TIMESTAMP                          NOT NULL,
    last_updated TIMESTAMP                          NOT NULL,
    content      TEXT                               NOT NULL,
    name         TEXT                               NOT NULL
);

INSERT INTO posts_migrate (id, user_id, task_id, created_at, last_updated, content, name)
SELECT id, user_id, task_id, created_at, last_updated, content, name
FROM posts;

DELETE
FROM posts;

ALTER TABLE posts
    ADD COLUMN type TEXT NOT NULL;

INSERT INTO posts (id, user_id, task_id, created_at, last_updated, content, name, type)
SELECT pm.id, pm.user_id, pm.task_id, pm.created_at, pm.last_updated, pm.content, pm.name, 'POST'
FROM posts_migrate pm;

DROP TABLE posts_migrate;

COMMIT;
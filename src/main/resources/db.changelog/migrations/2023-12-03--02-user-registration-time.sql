BEGIN
TRANSACTION;

CREATE TABLE users_migrate
(
    id         INT DEFAULT nextval('user_id_seq') NOT NULL PRIMARY KEY,
    name       TEXT                               NOT NULL,
    email      TEXT                               NOT NULL,
    role       TEXT                               NOT NULL,
    password   TEXT                               NOT NULL,
    is_deleted BOOLEAN                            NOT NULL
);

INSERT INTO users_migrate
SELECT id, name, email, role, password, is_deleted
FROM users;

DELETE FROM users;

ALTER TABLE users
    ADD COLUMN registration_time TIMESTAMP NOT NULL;

INSERT INTO users (id, name, email, role, password, is_deleted, registration_time)
SELECT id, name, email, role, password, is_deleted, CURRENT_TIMESTAMP
FROM users_migrate;

DROP TABLE users_migrate;

COMMIT;
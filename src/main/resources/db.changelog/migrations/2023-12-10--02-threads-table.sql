BEGIN
TRANSACTION;

CREATE SEQUENCE threads_id_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE threads
(
    id           INT     DEFAULT nextval('threads_id_seq') NOT NULL PRIMARY KEY,
    user_id      INT                                       NOT NULL,
    comment_id   INT                                       NOT NULL,
    content      TEXT                                      NOT NULL,
    created_at   TIMESTAMP                                 NOT NULL,
    last_updated TIMESTAMP                                 NOT NULL,
    is_anonymous BOOLEAN DEFAULT FALSE                     NOT NULL
);

ALTER TABLE threads
    ADD CONSTRAINT threads_users_fkey FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;
ALTER TABLE threads
    ADD CONSTRAINT threads_comments_fkey FOREIGN KEY (comment_id) REFERENCES comments (id) ON DELETE CASCADE;

COMMIT;
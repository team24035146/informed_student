CREATE SEQUENCE category_type_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE course_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE subject_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE task_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE user_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE post_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE comment_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE files_id_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE courses
(
    id   INT DEFAULT nextval('course_id_seq') NOT NULL PRIMARY KEY,
    name TEXT                                 NOT NULL UNIQUE
);

CREATE TABLE subjects
(
    id        INT DEFAULT nextval('subject_id_seq') NOT NULL PRIMARY KEY,
    course_id INT                                   NOT NULL,
    name      TEXT                                  NOT NULL UNIQUE
);

CREATE TABLE tasks
(
    id         INT DEFAULT nextval('task_id_seq') NOT NULL PRIMARY KEY,
    subject_id INT                                NOT NULL,
    type       TEXT                               NOT NULL,
    title      TEXT                               NOT NULL UNIQUE
);

CREATE TABLE users
(
    id       INT DEFAULT nextval('user_id_seq') NOT NULL PRIMARY KEY,
    name     TEXT                               NOT NULL UNIQUE,
    email    TEXT UNIQUE,
    role     TEXT,
    password TEXT
);

CREATE TABLE posts
(
    id           INT DEFAULT nextval('post_id_seq') NOT NULL PRIMARY KEY,
    user_id      INT                                NOT NULL,
    task_id      INT                                NOT NULL,
    name         TEXT                               NOT NULL UNIQUE,
    created_at   TIMESTAMP                          NOT NULL,
    last_updated TIMESTAMP                          NOT NULL,
    content      TEXT                               NOT NULL
);

CREATE TABLE files
(
    id      INT DEFAULT nextval('files_id_seq') NOT NULL PRIMARY KEY,
    post_id INT,
    name    TEXT                                NOT NULL,
    type    TEXT                                NOT NULL,
    file    BYTEA                               NOT NULL
);

CREATE TABLE comments
(
    id           INT DEFAULT nextval('comment_id_seq') NOT NULL PRIMARY KEY,
    user_id      INT                                   NOT NULL,
    post_id      INT                                   NOT NULL,
    content      TEXT                                  NOT NULL,
    created_at   TIMESTAMP                             NOT NULL,
    last_updated TIMESTAMP                             NOT NULL
);

ALTER TABLE tasks
    ADD CONSTRAINT tasks_subjects_fkey FOREIGN KEY (subject_id) REFERENCES subjects (id) ON DELETE CASCADE;
ALTER TABLE subjects
    ADD CONSTRAINT subjects_courses_fkey FOREIGN KEY (course_id) REFERENCES courses (id) ON DELETE RESTRICT;
ALTER TABLE posts
    ADD CONSTRAINT posts_users_fkey FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;
ALTER TABLE posts
    ADD CONSTRAINT posts_tasks_fkey FOREIGN KEY (task_id) REFERENCES tasks (id) ON DELETE CASCADE;
ALTER TABLE comments
    ADD CONSTRAINT comments_users_fkey FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;
ALTER TABLE comments
    ADD CONSTRAINT comments_posts_fkey FOREIGN KEY (post_id) REFERENCES posts (id) ON DELETE CASCADE;
ALTER TABLE files
    ADD CONSTRAINT files_post_fkey FOREIGN KEY (post_id) REFERENCES posts (id) ON DELETE CASCADE;
BEGIN
TRANSACTION;

ALTER TABLE posts ALTER COLUMN task_id DROP NOT NULL;

COMMIT;
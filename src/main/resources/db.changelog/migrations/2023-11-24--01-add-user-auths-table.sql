CREATE SEQUENCE user_auth_id_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE user_auths
(
    id       INT DEFAULT nextval('user_auth_id_seq') NOT NULL PRIMARY KEY,
    user_id  INT                                     NOT NULL,
    email    TEXT                                    NOT NULL UNIQUE,
    password TEXT                                    NOT NULL
);

DELETE FROM users;

ALTER TABLE users
    DROP COLUMN email, DROP COLUMN password;

ALTER TABLE user_auths
    ADD CONSTRAINT user_auths_users_fkey FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;
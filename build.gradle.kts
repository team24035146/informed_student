import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.10"
    id("org.springframework.boot") version "3.1.4"
    id("io.spring.dependency-management") version "1.0.15.RELEASE"
    java
    application
}

group = "team2"
version = "1.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

val springBootVersion = "3.1.4"
val lombokVersion = "1.18.30"
val javaVersion = "17"
val postgresqlVersion = "42.6.0"
val liquibaseVersion = "4.25.0"
val springdocOpenapiVersion = "2.2.0"
val mapstructVersion = "1.5.5.Final"
val lombokMapstructBindingVersion = "0.2.0"
val testcontainersVersion = "1.19.2"
val jsonWebTokenVersion = "0.12.3"
val quartzSchedulerVersion = "2.3.0"
val hibernateEnversVersion = "6.4.0.Final"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web:$springBootVersion")
    implementation("org.springframework.boot:spring-boot-starter-validation:$springBootVersion")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:$springBootVersion")
    implementation("org.springframework.boot:spring-boot-starter-security:$springBootVersion")
    implementation("org.postgresql:postgresql:$postgresqlVersion")
    implementation("org.liquibase:liquibase-core:$liquibaseVersion")
    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:$springdocOpenapiVersion")
    implementation("org.mapstruct:mapstruct:$mapstructVersion")
    implementation("org.projectlombok:lombok:$lombokVersion")
    implementation("org.projectlombok:lombok-mapstruct-binding:$lombokMapstructBindingVersion")
    implementation("io.jsonwebtoken:jjwt-api:$jsonWebTokenVersion")
    implementation("org.quartz-scheduler:quartz:$quartzSchedulerVersion")
    implementation("org.quartz-scheduler:quartz-jobs")
    implementation("org.springframework.boot:spring-boot-starter-quartz")
    implementation("org.hibernate.orm:hibernate-envers:$hibernateEnversVersion")


    testImplementation("org.springframework.boot:spring-boot-starter-test:$springBootVersion")
    testImplementation("org.testcontainers:junit-jupiter:$testcontainersVersion")

    annotationProcessor("org.projectlombok:lombok:$lombokVersion")
    annotationProcessor("org.mapstruct:mapstruct-processor:$mapstructVersion")
    annotationProcessor("org.projectlombok:lombok-mapstruct-binding:$lombokMapstructBindingVersion")

    runtimeOnly("io.jsonwebtoken:jjwt-impl:$jsonWebTokenVersion")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:$jsonWebTokenVersion")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.jar{
    enabled = false
}

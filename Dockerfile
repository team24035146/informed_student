# Stage 1: Install Gradle and build the application
FROM openjdk:17-jdk-alpine as builder

# Set the working directory in the Docker image
WORKDIR /app

# Install Gradle
ENV GRADLE_VERSION 8.4
RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-${GRADLE_VERSION}-bin.zip
ENV GRADLE_HOME /opt/gradle/gradle-${GRADLE_VERSION}
ENV PATH $GRADLE_HOME/bin:$PATH

# Copy the source code into the Docker image
COPY . /app

# Build the application using the Gradle wrapper
RUN gradle clean build -x test --no-daemon

# Stage 2: Create the runtime image
FROM eclipse-temurin:17-jre-alpine

# Set the working directory in the Docker image
WORKDIR /app

# Copy the built jar file from the build stage into the runtime image
COPY --from=builder /app/build/libs/*.jar /app/app.jar

# Expose the port the application runs on
EXPOSE 8080

# Run the application
ENTRYPOINT ["java", "-jar", "/app/app.jar"]